import React from 'react';

import './styles.scss';

const DateItem = ({
    date,
    info,
}) => (
  <div className="importantDate">
    <h1>
      {date}
    </h1>
    <h2>
      {info}
    </h2>
  </div>
);

const ImportantDates = ({
    title,
    importantDateData,
}) => (
  <div className="importantDateWrapper">
    <h1 className="title">{title}</h1>
    { importantDateData.map(
      dateData => <DateItem date={dateData.date} info={dateData.info}/>
    )}
  </div>
)

export default ImportantDates;
