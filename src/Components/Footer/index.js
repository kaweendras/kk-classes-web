import React from 'react';

import WhatsApp from 'static/whatsapp.svg';
import FaceBook from 'static/facebook.svg';
import Phone from 'static/phone.svg';

import './styles.scss';

const Footer = () => (
  <div  className="footer">
    <div className="contactInfo">
      <div className="title">
        Contact Us
      </div>
      <a href="tel:7827173383">
        <img src={Phone} alt="Phone icon" />
      </a>
      <a href="https://wa.me/qr/RB6V3KW4GJCNB1">
        <img src={WhatsApp} alt="whatsapp icon" />
      </a>
      <a href="https://www.facebook.com/KK-Classes-110083153964654">
        <img src={FaceBook} alt="Facebook icon" />
      </a>
    </div>
    <small className="copyright">
      ©2020 KK Classes
    </small>
  </div>
)

export default Footer;
