import React from 'react';

import { Navbar, Nav, Container } from 'react-bootstrap';

import {
  Link,
} from 'react-router-dom';

import './styles.scss';

const NavBar = () => (
  <Navbar className="navbar" expand="md" variant="dark">
    <Container>
      <Link to="/">
        <Navbar.Brand className="brandLogo">
          KK Classes
          <div className="tagline">
            आपका लक्ष्य हमारा लक्ष्य
          </div>
        </Navbar.Brand>
      </Link>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link>
            <Link to="/">Home</Link>
          </Nav.Link>
          <Nav.Link>
            <Link to="/current-affairs">Current Affairs</Link>
          </Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Container>
  </Navbar>
 );

 export default NavBar;
