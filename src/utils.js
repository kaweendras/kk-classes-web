const padZero = (number) => {
  if (number) {
    return number.length === 1 ? `0${number}` : number
  }
}

export {
    padZero,
}
