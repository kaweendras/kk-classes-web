import React, { useState, useCallback } from 'react';
import { Redirect } from 'react-router';
import { useParams, useHistory } from 'react-router-dom';
import moment from 'moment';
import ReactAudioPlayer from 'react-audio-player';
import ReactMarkdown from 'react-markdown';

import ImportantDates from 'Components/ImportantDates';
import importantDates from 'static/importantDates.json';

import LeftArrow from 'static/icons/left.png';
import RightArrow from 'static/icons/right.png';

import { padZero } from 'utils.js';
import {
  Alert, Container
} from 'react-bootstrap';

import './styles.scss';

const CURRENT_AFFAIRS_UPLOAD_TIME = 19; 

const CurrentAffairs = () => {
  const history = useHistory();
  let { date, month, year } = useParams();

  const gotoDate = useCallback(
    (day) => (
      moment(`${date}/${month}/${year}`, 'D/M/YYYY').add(day, "days").format('D/M/YYYY').split('/')
    ), [date, month, year]
  );

  const [text, setText] = useState(null);

  let textFile = null;
  let audio = null;

  const currentAffairsDate = `${date}/${month}/${year}`

  if (!(date && month && year)) {
    // show the todays current affairs page
    const currentDate = moment();
    date = currentDate.date();
    month = currentDate.month() + 1;
    year = currentDate.year();
    
    if (currentDate.hour() < CURRENT_AFFAIRS_UPLOAD_TIME) {
      [date, month, year] = gotoDate(-1);
    }

    return (
      <Redirect to={`/current-affairs/${date}/${month}/${year}/`} />
    )
  }

  try {
    textFile = require(`../../CurrentAffairs/${year}/${month}/${date}/text.txt`);
    fetch(textFile).then(
      (result) => result.text()
    ).then(
      text => { setText(text) }
    );
    audio = require(`../../CurrentAffairs/${year}/${month}/${date}/audio.mp3`);
  } catch (error) {
    console.log(error)
  }

  const onDateConfirm = (newDate) => {
    const [year, month, day] = newDate.split('-');
    history.push(
      `/current-affairs/${parseInt(day)}/${parseInt(month)}/${year}/`
    );
  }

  const changeDate = (newDate) => {
    history.push(
      `/current-affairs/${parseInt(newDate[0])}/${parseInt(newDate[1])}/${parseInt(newDate[2])}/`
    );
  }

  return (
    <>
      <div className="CANavigationWrapper">
        <Container className="CANavigation">
          <input type="date" name="current affairs"
            value={`${year}-${padZero(month)}-${padZero(date)}`}
            onInput={(e) => onDateConfirm(e.target.value)}
          />
          <div>
            <img
              onClick={() => changeDate(gotoDate(-1))} 
              src={LeftArrow}
              alt="Goto Previous"
            />
            <img 
              onClick={() => changeDate(gotoDate(1))} 
              src={RightArrow}
              alt="Goto Next"
            />
          </div>
        </Container>
      </div>
      <Container>
        <div className="current-affairs">
          {!textFile && !audio ?
            <Alert className="no-data" variant="warning" showIcon >{`No current affairs available for ${currentAffairsDate}`} </Alert> :
            <>
              <div className="audio-player">
                <h1><span role="img" aria-label="Recording">🎤</span> Recording
                <span className="date">
                  </span></h1>
                <ReactAudioPlayer
                  src={audio}
                  controls
                />
              </div>
              <div className="notes">
                <h1>
                  <span role="img" aria-label="Current Affairs">📚</span> Current Affairs
                </h1>
                <Alert variant="primary">
                  Note: पूरा विस्तार से समझने के लिए रिकॉर्डिंग सुने
                </Alert>
                <ReactMarkdown source={text} />
                <div className="importantDates">
                  <ImportantDates
                    title={"Important Dates"}
                    importantDateData={importantDates[month]}
                  />
                </div>
                <Alert variant="primary">
                  Note: इसका उत्तर प्रतिदिन KK Classes Whatsapp Group में दोपहर 12:00 बजे भेजा जाता है।
                </Alert>
              </div>
            </>
          }
        </div>
      </Container>
    </>
  );
}

export default CurrentAffairs;
