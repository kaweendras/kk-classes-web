import React from 'react';

import {
  Container,
  Card,
} from 'react-bootstrap';

import ImgArmy from 'static/Army.jpg';
import ImgIAF1 from 'static/IAF1.jpg';
import ImgNavy from 'static/IndianNavy.jpg';
import ImgNavy1 from 'static/IndianNavy1.jpg';
import ImgBSF from 'static/BSF.webp';
import ImgITBP from 'static/ITBP.jpg';
import ImgCISF from 'static/CISF.jpg';
import ImgASR from 'static/ASR.jpg';
import ImgASR1 from 'static/ASR1.jpg';
import ImgNSG from 'static/NSG.jpg';

import HeroCarousel from 'Components/HeroCarousel';
import Quotes from 'static/quotes.json';

import './styles.scss';

const Images = [
  {
    src: ImgArmy,
    title: "Indian Army",
  },
  {
    src: ImgIAF1,
    title: "Indian Air Force",
  },
  {
    src: ImgNavy1,
    title: "Indian Navy",
  },
  {
    src: ImgNavy,
    title: "Indian Navy",
  },
  {
    src: ImgBSF,
    title: "Border Security Force",
  },
  {
    src: ImgITBP,
    title: "Indo-Tibetan Border Police",
  },
  {
    src: ImgCISF,
    title: "Central Industrial Security Force",
  },
  {
    src: ImgASR,
    title: "Assam Rifles",
  },
  {
    src: ImgASR1,
    title: "Assam Rifles",
  },
  {
    src: ImgNSG,
    title: "National Security Guard",
  },
];

const Home= () => {
  return (
    <>
      <HeroCarousel
        Images={Images}
      />
      <Container className="mt-5 quoteCard">
      <Card>
        <Card.Header>Quote of the day</Card.Header>
        <Card.Body>
          <Card.Text>
            { Quotes[Math.floor(Math.random() * Quotes.length)].text }
          </Card.Text>
        </Card.Body>
      </Card>
      </Container>
    </>
  );
}

export default Home;
