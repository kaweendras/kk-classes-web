import React from 'react';
import {
  HashRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';

import './App.scss';

import CurrentAffairs from './Containers/CurrentAffairs';
import Home from './Containers/Home';
import AboutUs from './Containers/AboutUs';
import NavBar from './Components/NavBar';
import Footer from './Components/Footer';

const App = () => (
  <Router>
    <div className='main-section'>
      <NavBar/>
      <Switch>
        <Route path="/about-us" component={AboutUs} />
        <Route path="/current-affairs/:date?/:month?/:year?/" component={CurrentAffairs} />
        <Route path="/" component={Home} />
      </Switch>
    </div>
    <Footer />
  </Router>
);

export default App;
