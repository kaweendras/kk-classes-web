

  ## 1. Amnesty International(एमीनेस्टी इंटरनेशनल) ने भारत में काम करना बंद कर दिया है
![India World map | India world map, India map, India](https://i.pinimg.com/originals/d7/12/a7/d712a723298f449e0aace1ef58dbc165.jpg)

  ## 2. ISRO ने 2025 वर्ष में अपना Venus(शुक्र) मिशन शुरू करने की घोषणा की है
![Solar System - Wikipedia](https://upload.wikimedia.org/wikipedia/commons/c/cb/Planets2013.svg)

  ## 3. दक्षिण पूर्व रेलवे ने ऑपरेशन मेरी सहेली शुरू किया है
  ![Zones and divisions of Indian Railways - Wikipedia](https://upload.wikimedia.org/wikipedia/commons/b/b1/Indianrailwayzones-numbered.png)
 ___
 # आज का सवाल 

 ### **योजना आयोग के उपाध्यक्ष को भारत सरकार के सरकारी वरीयता क्रम में किस महत्त्व का दर्जा दिया गया है ?**
![Central Secretariat buildings, New Delhi, India - Students | Britannica  Kids | Homework Help](https://cdn.britannica.com/55/131355-004-5CA6F4B6.jpg)
