

  ## 1. FATF ने पाकिस्तान देश को अपनी ग्रे सूचि में रखा है
![Pakistan Physical Map](https://www.freeworldmaps.net/asia/pakistan/pakistan-map-physical.jpg)

  ## 2. बांग्लादेश देश ने दुष्कर्म मामलो में मृत्युदंड का प्रवाधान किया है
![Oxfam to help over 200,000 Rohingya in Bangladesh | Oxfam](https://assets.oxfamamerica.org/media/images/Bangladesh-map-09-2017-aa_VK.2e16d0ba.fill-1180x738-c100.png)
 ___
 # आज का सवाल 

 ### **सरकारिया समिति किस विषय वस्तु से सम्बंधित है ?**
![Central State Relation - Legislative, Administrative and Financial](https://img.latestgkgs.com/2018/11/18/central-state-relation-1542524461.jpg)