

  ## 1. एवा मूंटो फ़िनलैंड देश की एक दिन की प्रधानमंत्री बनी है
![PM for a day: Teenager fills Finland's top job | Life | Malay Mail](https://media2.malaymail.com/uploads/articles/2020/2020-10/marins0810.jpg)

  ## 2. विजया राजे संधिया के सम्मान में 100 रुपए का स्मारक सिक्का ज़ारी किया गया है 
![Madhya Pradesh: PM Modi pays tributes to Vijaya Raje Scindia, releases  commemorative coin](https://gumlet.assettype.com/freepressjournal%2F2020-10%2F01c95da7-56d8-4f32-9301-c1c2d2049315%2F1210_20201012039l.jpg?auto=format%2Ccompress&w=1200)

  ## 3. जी बी एस सिद्धू ने खालिस्तान कांस्पीरेसी नामक पुस्तक लिखी है  
![The Khalistan Conspiracy' to decode latent facts about 1984: Former RAW  Officer GBS Sidhu - TrueScoop](https://tsncdn.truescoopnews.com/images/full/newsinner_20201010111811_1.jpg)

  ## 4. राफेल नडाल(स्पेन) ने 2020 का पुरुष फ्रेंच ओपन ख़िताब जीता है
![Rafael Nadal](https://thumbor.forbes.com/thumbor/fit-in/416x416/filters%3Aformat%28jpg%29/https%3A%2F%2Fspecials-images.forbesimg.com%2Fimageserve%2F5ece8a5c938ec500060aae37%2F0x0.jpg%3Fbackground%3D000000%26cropX1%3D503%26cropX2%3D2965%26cropY1%3D156%26cropY2%3D2616)

  ## 5. इगा सवीआटेक(पोलैंड)  ने 2020 का महिला फ्रेंच ओपन ख़िताब जीता है
![Iga Swiatek Cruises Into French Open Final, Where She Will Be Hard to Beat  - The New York Times](https://static01.nyt.com/images/2020/10/08/sports/08french-womens-semis01/merlin_178226100_4da48107-e604-4bfd-b5cb-13779263cb09-mobileMasterAt3x.jpg)

  ## 6. DU-दिल्ली यूनिवर्सिटी पूरी तरह से ऑनलाइन प्रवेश प्रक्रिया शुरू करने वाला पहला विश्वविद्यालय बना है 
![Why Study Liberal Arts at Delhi University – Q&A With Utsa Bose, Department  of English, St. Stephen's College |](https://www.stoodnt.com/blog/wp-content/uploads/2020/01/DU.jpg)

 ___
 # आज का सवाल 

 ### **भारत में योजना से सम्बंधित सबसे पहला विचार प्रस्तुत करने का श्रेय किसे जाता है ?**
![M. Visvesvaraya - Wikipedia](https://upload.wikimedia.org/wikipedia/commons/4/44/Vishveshvarayya_in_his_30%27s.jpg)