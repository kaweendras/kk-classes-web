

  ## 1. हाल ही में जारी ग्लोबल इनोवेशन इंडेक्स 2020 में स्विट्ज़रलैंड देश शीर्ष स्थान पर रहा है
![Switzerland map | Pre-Designed Illustrator Graphics ~ Creative Market](https://images.creativemarket.com/0.1.0/ps/5608156/1820/1214/m1/fpnw/wm1/switzerland_map-.jpg?1545657784&s=5a44ea063845548c5949ce13eb51191c)

  ## 2. हाल ही में हैदराबाद अंतराष्ट्रीय एयरपोर्ट ने CII-GBC नेशनल एनर्जी लीडर पुरस्कार जीता है 
![MAP: What Telangana state will look like - Rediff.com India News](https://im.rediff.com/news/2013/jul/30look1.jpg)

  ## 3. हाल ही में सेरेना विलियम(USA) टेनिस खेल में सबसे अधिक मैच जितने वाली खिलाडी बन गयी है
![Serena Williams and Daughter Olympia Wear Matching Princess Dresses](https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/serena-williams-of-the-us-with-her-daughter-alexis-olympia-news-photo-1596814464.jpg)

  ## 4. हाल ही में बांग्लादेश ने हमारे पूर्व राष्ट्रपति प्रणब मुख़र्जी के निधन पर एक दिन के राष्ट्रीय शोक की घोषणा की है 
![Epaar Bangla, opaar Bangla | Dhaka Tribune](https://media-eng.dhakatribune.com/uploads/2019/06/partha-bigstock-1560767749247.jpg)

  ## 5. हाल ही में एम वीरलक्ष्मी को भारत की पहली महिला एम्बुलेंस ड्राइवर के रूप में नियुक्त किया गया है 
![Tamil Nadu Appoints India's First Woman Ambulance Driver](https://thelogicalindian.com/h-upload/2020/09/02/180837-veeralakshmiweb.jpg)

 ___
 # आज का सवाल 

 ### **भारत के प्रथम उपराष्ट्रपति कौन थे ?**
 ![VP Venkaiah Naidu makes clarion call for 100% literacy in India - Republic  World](https://img.republicworld.com/republic-prod/stories/promolarge/xxhdpi/swacfuoolldybb6l_1578689563.jpeg?tr=w-812,h-464)