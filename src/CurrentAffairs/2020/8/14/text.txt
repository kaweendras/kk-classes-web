

 ## 1. 12 अगस्त को हमने अंतराष्ट्रीय युवा दिवस मनाया
![International Youth Day 2020 - August 12](https://affairscloud.com/assets/uploads/2020/08/International-Youth-Day.jpg)

 ## 2. वेंकैया नायडू(उपराष्ट्रपति) ने Connecting Communicating Changing नामक पुस्तक लिखा है 
![Vice President Venkaiah Naidu greets people on Independence Day ...](https://resize.indiatvnews.com/en/resize/newbucket/1200_-/2019/08/vp-naidu-calls-on-seychelles-president-ffac6a12-7897-11e8-b46a-be68571826e9-1565806531.jpg)

 ## 3. हाल ही में इजराइल देश ने एम्स दिल्ली के साथ कृतिम बुद्धि आधारित तकनीक साझा की है
![israel-map | Haifa, Amman, Tel aviv](https://i.pinimg.com/originals/bd/cf/b0/bdcfb031da72307874ecb7805790bb6b.jpg)

 ## 4. हाल ही में मुंबई ट्रैफिक सिगनल पर फीमेल आइकॉन का इस्तेमाल करने वाला भारत का पहला शहर बन गया है
![Lady in red: Mumbai gets female figure signage at traffic signals ...](https://www.deccanherald.com/sites/dh/files/articleimages/2020/08/02/mumbai%20%281%29-1596355425.jpg)

 ## 5. हाल ही में सुदर्शन साहू(मूर्तिकार) को ओडिशा ललित कला अकादमी द्वारा धरमपद पुरस्कार प्रदान किया गया है 
 ![live demonstration of stone carving](https://lh3.googleusercontent.com/proxy/xTwGAyZCXJBm42pyYy0ql6UN4AAMVC2qIBWuuuLfA6E5o3Mh-JP9ViXuz6KhZ4_8GEOIY-T4OQ9lKKMmFAZkOtcBug124T_a2DKAIoWFY8JMVn5D5Z_NIQzZvPZw1xY)
 
 ___
 # अगस्त के महत्तवपूर्ण दिन
 ### 09= **भारत छोड़ो दिवस-आदिवासी दिवस**
 ### 12= **अंतर्राष्ट्रीय युवा दिवस(2000)**
 ### 29= **राष्ट्रीय खेल दिवस**

 ___
 # आज का सवाल 

 ### **स्वामी विवेकानंद का मूल नाम क्या था ?**
![Top 30+ New Swami Vivekananda Quotes in Hindi अनमोल विचार](https://gr8learnings.com/wp-content/uploads/2019/11/swami-vivekananda-quotes-hindi-gr8learnings.com-1.png)