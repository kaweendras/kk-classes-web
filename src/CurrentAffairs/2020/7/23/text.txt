
 ## 1. सूरज कदे मरदा नहीं ये पुस्तक बलदेव सिंह ने लिखी है
![Top Current Affairs 21 July 2020](https://affairscloud.com/assets/uploads/2020/07/New-book-%E2%80%98Suraj-Kade-Marda-Nahi%E2%80%99-sun-never-dies-on-Udham-Singh-by-Akademi-awardee-Baldev-Singh-Sadaknama.jpg)

 ## 2. भारत और अमेरिका देश की नौसेना के बीच PASSEX युद्ध अभ्यास आयोजित किया गया है 
![File:Passage Exercise (PASSEX) during International Fleet Review ...](https://upload.wikimedia.org/wikipedia/commons/c/c7/Passage_Exercise_%28PASSEX%29_during_International_Fleet_Review_2016_%2805%29.jpg)

 ## 3. मध्य प्रदेश के राज्यपाल लालजी टंडन का निधन हो गया है
 ![Lalji Tandon in Memories: As a Madhya Pradesh Governor Lalji ...](https://images.jagran.com/naidunia/ndnimg/21072020/21_07_2020-lalji_tandon_in_memories.jpg)
 
 ## 4. छत्तीसगढ़ राज्य सरकार ने गोधन न्याय योजना शुरू किया है
 ![गोधन न्याय योजना- गोपालकों से गोबर ...](https://www.yojanaschemehindi.com/wp-content/uploads/2020/07/Godhan-Nyay-Yojana-CG.jpg)
 
 ___
 # जुलाई के महत्तवपूर्ण दिन
 ### 01= **SBI स्थापना/डॉक्टर्स/GST दिवस**
 ### 04= **अमेरिका स्वतंत्रता दिवस(1776)**
 ### 11= **विश्व जनसँख्या दिवस**
 ### 18= **नेल्सन मंडेला अंतर्राष्ट्रीय दिवस**
 ###  26= **कारगिल विजय दिवस**
 ###  29= **विश्व बाघ दिवस(1973)**

 ___
 # आज का सवाल 

 ### **सिंधु घाटी सभ्यता में घोड़े के साक्ष्य कहा से मिले है ?**
![Indus Valley Civilisation - Wikipedia](https://upload.wikimedia.org/wikipedia/commons/c/c9/Indus_Valley_Civilization%2C_Mature_Phase_%282600-1900_BCE%29.png)